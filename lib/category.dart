import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/material.dart';
import './popup.dart';
import './calculator.dart';
import './location.dart';
class Category{
  final String name;
  final Widget icon;
  final Widget child;
  Category({this.child, this.name, this.icon});
}
final List<Category> categories = [
  Category(child: BADpage(), name:"Profile", icon: Icon(FontAwesomeIcons.male)),
  Category(child: Calcpage(), name:"Calculator", icon: Icon(FontAwesomeIcons.calculator)),
  Category(child: LocationPage(), name:"Location", icon: Icon(FontAwesomeIcons.searchLocation)),
];