import 'package:flutter/material.dart';
import 'package:bottomreveal/bottomreveal.dart';

class BADpage extends StatefulWidget {
  BADpage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}
class Person {
  String name;
  String bornYear;
  String sex;
  Person({this.name, this.bornYear, this.sex});
}
class _MyHomePageState extends State<BADpage> {
  List<Person> users = [];
  String name = '';
  String age = '';
  bool editOpen = false;
  int editIndex;
  TextEditingController nameController = TextEditingController();

  void handleEdit(int edit) {
    setState(() {
      editIndex = edit;
      editOpen = true;
    });
  }

  void handleSaveAge() {
    List<Person> turZuuriinList = users;
    users[editIndex].bornYear = age;
    setState(() {
      users = turZuuriinList;
      editOpen = false;
    });
  }

  void handleSave() {
    List<Person> turZuuriinList = users;
    turZuuriinList.add(Person(name: name));
    setState(() {
      users = turZuuriinList;
      nameController.text = '';
    });
  }

  void handleRemove(int index) {
    List<Person> turZuuriinList = users;
    turZuuriinList.removeAt(index);
    setState(() {
      users = turZuuriinList;
    });
  }

  final BottomRevealController _menuController = BottomRevealController();
  @override
  Widget build(BuildContext context) {
    var screen = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: Text('Profiles'),
      ),
      body: BottomReveal(
        openIcon: Icons.close,
        closeIcon: Icons.close,
        revealWidth: 100,
        revealHeight: 100,
        backColor: Color(0xff2D0C3F),
        frontColor: Colors.grey.shade200,
        rightContent: _buildRightMenu(),
        bottomContent: _buildBottomContent(),
        controller: _menuController,
        body: Container(
          width: double.infinity,
          height: screen.height - 56 - MediaQuery.of(context).padding.top,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              editOpen
                  ? Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10, horizontal: 24),
                      child: TextField(
                        decoration: InputDecoration(
                          hintText: 'Enter your age',
                          suffixIcon: IconButton(
                            onPressed: () => handleSaveAge(),
                            icon: Icon(Icons.save, color: Colors.blue),
                          ),
                        ),
                        onChanged: (String nas) {
                          setState(() {
                            age = nas;
                          });
                        },
                      ),
                    )
                  : SizedBox(),
              Expanded(
                child: ListView.builder(
                  itemCount: users.length,
                  itemBuilder: (context, index) {
                    var sda = users[index];
                    return Container(
                      height: 48,
                      width: double.infinity,
                      padding: EdgeInsets.all(8),
                      margin: EdgeInsets.all(12),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Color(0xffF9E5BC)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text('Нэр: ${sda.name}'),
                              Text(sda.bornYear != null
                                  ? "Нас: ${sda.bornYear}"
                                  : ""),
                            ],
                          ),
                          Spacer(),
                          FlatButton(
                            child: Icon(Icons.delete),
                            onPressed: () => handleRemove(index),
                          ),
                          FlatButton(
                            child: Icon(Icons.edit),
                            onPressed: () => handleEdit(index),
                          )
                        ],
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  TextField _buildBottomContent() {
    return TextField(
      onChanged: (String value) {
        setState(() {
          name = value;
        });
      },
      style: TextStyle(color: Colors.white, fontSize: 18.0),
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.grey,
        contentPadding: const EdgeInsets.all(16.0),
        prefixIcon: Icon(
          Icons.search,
          color: Colors.white,
        ),
        border: OutlineInputBorder(
            gapPadding: 8.0,
            borderSide: BorderSide(color: Colors.grey),
            borderRadius: BorderRadius.circular(30.0)),
      ),
    );
  }

  Column _buildRightMenu() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        const SizedBox(height: 10.0),
        MaterialButton(
          height: 60.0,
          padding: const EdgeInsets.all(0),
          textColor: Colors.white,
          minWidth: 60,
          child: Icon(
            Icons.check,
            size: 30,
          ),
          color: Color(0xff644B77),
          elevation: 0,
          onPressed: () {
            handleSave();
            _menuController.close();
          },
        ),
      ],
    );
  }
}
