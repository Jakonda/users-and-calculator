import 'package:flutter/material.dart';

class Calcpage extends StatefulWidget {
  Calcpage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}
class _MyHomePageState extends State<Calcpage> {
  String result = '';
  String additional = '';
  bool isAdding;
  bool isMultiply;
  static const buttons = ['7', '8', '9', '4', '5', '6', '1', '2', '3', '0'];

  void handleNumberClick(String addition) {
    if (isAdding == null && isMultiply == null) {
      setState(() {
        result = result + addition;
      });
    } else {
      setState(() {
        additional = additional + addition;
      });
    }
  }

  void handleAdd() {
    setState(() {
      isAdding = true;
      isMultiply = null;
    });
  }

  void handleMinus() {
    setState(() {
      isAdding = false;
      isMultiply = null;
    });
  }

  void handleDivide() {
    setState(() {
      isMultiply = false;
      isAdding = null;
    });
  }

  void handleMultiply() {
    setState(() {
      isMultiply = true;
      isAdding = null;
    });
  }

  void handleClear() {
    setState(() {
      result = '';
      isMultiply = null;
      isAdding = null;
      additional = '';
    });
  }

  returnSymbol() {
    if (isAdding != null || isMultiply != null) {
      if (isAdding == null) {
        if (isMultiply) {
          return ' * ';
        } else {
          return ' / ';
        }
      } else {
        if (isAdding) {
          return ' + ';
        } else {
          return ' - ';
        }
      }
    } else {
      return '';
    }
  }

  void handleCalculate() {
    if (result != '' || additional != '') {
      var first = double.parse(result);
      var second = double.parse(additional);
      if (isAdding != null || isMultiply != null) {
        if (isAdding == null) {
          if (isMultiply) {
            setState(() {
              result = (first * second).toString();
              isAdding = null;
              isMultiply = null;
              additional = '';
            });
          } else {
            setState(() {
              result = (first / second).toString();
              isAdding = null;
              isMultiply = null;
              additional = '';
            });
          }
        } else {
          if (isAdding) {
            setState(() {
              result = (first + second).toString();
              isAdding = null;
              isMultiply = null;
              additional = '';
            });
          } else {
            setState(() {
              result = (first - second).toString();
              isAdding = null;
              isMultiply = null;
              additional = '';
            });
          }
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var screen = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("Only for Geniuses"),
        backgroundColor: Colors.deepPurple,
              elevation: 0,
      ),
      body: Center(
        child: Container(
          alignment: Alignment.center,
          width: screen.width,
          height: screen.height / 1.5,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.symmetric(vertical: 18),
                child: Center(
                  child: Text(
                    result == '' ? '0' : result + returnSymbol() + additional,
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.w600),
                  ),
                ),
                width: screen.width / 2,
                height: 50,
                color: Colors.white,
              ),
              Container(
                width: double.infinity,
                height: 300,
                child: GridView.builder(
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      childAspectRatio: 3,
                      mainAxisSpacing: 25,
                      crossAxisSpacing: 5,
                      crossAxisCount: 3),
                  itemCount: buttons.length,
                  itemBuilder: (BuildContext context, int index) {
                    return SizedBox(
                      width: 60,
                      height: 18,
                      child: RaisedButton(
                          child: Text(buttons[index]),
                          onPressed: () => handleNumberClick(buttons[index])),
                    );
                  },
                ),
              ),
              SizedBox(height: 24),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  RaisedButton(
                    child: Text('+'),
                    onPressed: () {
                      handleAdd();
                    },
                  ),
                  RaisedButton(
                    child: Text('-'),
                    onPressed: () {
                      handleMinus();
                    },
                  ),
                  RaisedButton(
                    child: Text('='),
                    onPressed: () {
                      handleCalculate();
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  RaisedButton(
                    child: Text('*'),
                    onPressed: () {
                      handleMultiply();
                    },
                  ),
                  RaisedButton(
                    child: Text('/'),
                    onPressed: () {
                      handleDivide();
                    },
                  ),
                  RaisedButton(
                    child: Text('C'),
                    onPressed: () {
                      handleClear();
                    },
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}